import { EAuthProviders } from './../../EAuth';
import { AngularFireAuth } from 'angularfire2/auth';
import { Component, ChangeDetectorRef } from '@angular/core';
import { NavController } from 'ionic-angular';
import firebase from 'firebase';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  providers = EAuthProviders;

  user = {
    loggedin: false,
    name: '',
    profilePicture: '',
    email: '',
    provider: EAuthProviders.facebook
  };

  constructor(
    public navCtrl: NavController,
    private fire: AngularFireAuth,
    private detectorRef: ChangeDetectorRef
  ) {}

  login(authProvider: EAuthProviders) {
    // this.fire.auth.signInWithPopup(this.getProvider(authProvider)).then(res => {
    this.fire.auth
      .signInWithRedirect(this.getProvider(authProvider))
      .then(() => {
        this.fire.auth.getRedirectResult().then(res => {
          console.log(res);

          this.user.loggedin = true;
          this.user.name = res.user.displayName;
          this.user.email = res.user.email;
          this.user.profilePicture = res.user.photoURL;
          this.user.provider = authProvider;

          // manually run a cicle for the component
          this.detectorRef.detectChanges();
        }).catch(exc => console.log(exc));
      }).catch(e => console.log(e));
  }

  logout() {
    this.fire.auth.signOut();
    this.user.loggedin = false;

    // manually run a cicle for the component
    this.detectorRef.detectChanges();
  }

  private getProvider(provider: EAuthProviders) {
    switch (provider) {
      case EAuthProviders.facebook:
        return new firebase.auth.FacebookAuthProvider();
      case EAuthProviders.google:
        return new firebase.auth.GoogleAuthProvider();
      case EAuthProviders.twitter:
        return new firebase.auth.TwitterAuthProvider();
      case EAuthProviders.github:
        return new firebase.auth.GithubAuthProvider();
      default:
        return new firebase.auth.FacebookAuthProvider();
    }
  }
}
